const express = require('express');
const product = require('../controllers/product');
const router = express.Router();

// redirect to home
router.get('/', (req, res) => res.redirect('/product/index'));

router.get('/product/create', product.create);
router.post('/product/store', product.store);
router.get('/product/index', product.index);
router.get('/product/:id/details', product.show);
router.get('/product/:id/edit', product.edit);
router.put('/product/:id/update', product.update);
router.delete('/product/:id/destroy', product.destroy);

module.exports = router;
