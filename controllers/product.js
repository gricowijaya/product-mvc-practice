const { Product } = require('../models')

module.exports = { 
    // create for frontend 
   create: async(req, res, next) => { return res.render('product/create'); },
    
    // all product
    index: async(req, res, next) => { 
        try {
            const products = await Product.findAll();
            return res.render('product/index', {products});
        } catch(err) { 
            next(err)
        }
    },

    // details of product
    show: async(req, res, next) => { 
        try { 
            const {id} = req.params

            const product = await Product.findOne({where: {id}});
            return res.render('product/details', {product}); 
        } catch(err) { 
            next(err)
        }
    },

    // store into the database
    store: async (req, res, next) => {
        try { 
            const {name, price} = req.body
            // const product = await Product.create({name, price});
            console.log(req.body);
            // res.send(product);
            // return res.render('product/details', {product});
        } catch(err) { 
            next(err)
        }
    },

    // store into the database
    edit: async (req, res, next) => {
        try { 
            const {name, price} = req.body
            const product = await Product.create({name, price});
            // res.send(product);
            return res.render('product/details', {product});
        } catch(err) { 
            next(err)
        }
    },

    update: async (req, res, next) => {
        try { 
            const {id} = req.params
            const {name, price} = req.body
            const product = await Product.update({name,price},{where: {id} });
            return res.redirect('product/{id}/details', {product});
        } catch(err) { 
            next(err)
        }
    },

    // delete database
    destroy: async (req, res, next) => {
        try { 
            const {id} = req.params
            const product = await Product.destroy({where:{id}});
            res.send(product);
            // return res.render('product/details', {product});
        } catch(err) { 
            next(err)
        }
    }
}
