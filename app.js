require('dotenv').config();
const express = require('express');
const morgan = require('morgan');
const router = require('./routes');
const app = express();
const methodOverride = require('method-override')

const {
    HTTP_PORT = 3000
} = process.env;

app.use(express.json());
app.use(express.urlencoded({extended:true})); // url encoded harus diatas
app.use(methodOverride("_method"))
app.use(morgan('dev'));
app.use(router)
app.set('view engine', 'ejs');

// 404 handler
app.use((req, res, next) => {
    return res.status(404).json({
        status: false,
        message: 'Are you lost?'
    });
});

// 500 handler
app.use((err, req, res, next) => {
    console.log(err);
    return res.status(500).json({
        status: false,
        message: err.message
    });
});

app.listen(HTTP_PORT, () => console.log('listening on port', HTTP_PORT));
